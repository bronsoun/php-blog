<header>
    <nav>
        <?php if( isset($_SESSION['user_login']) ): ?>
            <a href = "index.php">Main page</a>
            <a href = "posts.php">Posts</a>
            <a href = "newpost.php">Create post</a>
        <?php else: ?>
            <a href = "signin.php">Authorize</a>
            <a href = "signup.php">Registrate</a>
        <?php endif; ?>
    </nav>
</header>
