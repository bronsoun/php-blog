<?php require_once 'logic/db.php'; ?>

<!DOCTYPE html>
<html lang="en">
<html>
<head>
<link rel="stylesheet" href="style.css">
</head>
<body>
	<?php include_once 'parts/header.php';?>
	<?php if( isset($_SESSION['user_login'])): ?>
	
	<section id='posts-sec'>
	<?php include_once 'logic/printpost.php'; ?>
	</section>
	<?php else: ?>
	<h1>Access Denied</h1>
	<h2>PLease, <a href='signin.php'>authorize</a>
	or <a href='signup.php'>register</a></h2>
<?php endif; ?>
</body>
</html>