<?php
require_once 'db.php';

$login = trim($_POST['login']);
$pwd = trim($_POST['pwd']);

if( !empty($login) && !empty($pwd) ){
     
    $sql_check = 'SELECT EXISTS( SELECT login FROM users WHERE login = :login )';
    $stmt_check = $pdo->prepare($sql_check);
    $paramsLogin = [':login' => $login];
	$stmt_check->execute($paramsLogin);
    
    if ($stmt_check->fetchColumn() ){
		die('User exists');
    }
    
    $pwd = password_hash($pwd, PASSWORD_DEFAULT);

    $sql = 'INSERT INTO users(login, password) VALUES(:login, :pwd)';
    $params = ['login' => $login, ':pwd' => $pwd];
    
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);

    echo 'Registration success!';

}else{
    echo 'Pleae, fill in all forms';
}

?>
<br>
<a href="../signin.php">Authorization page</a>